source('cargar_datos.R')

datos_test = promedios_n_dias_sin_domingos

qqnorm(datos_test)
qqline(datos_test, col = "red")

r = shapiro.test(datos_test)
k = ks.test(datos_test,"pnorm",mean(datos_test),sd(datos_test))
